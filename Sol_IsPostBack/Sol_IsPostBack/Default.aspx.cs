﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_IsPostBack
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page.IsPostBack==false)
            {
                Response.Write("Hello");

                //bind labelName
                lblName.Text = "Hello Again";

                //bind span tag
                spanName.InnerText = "Hello span";
                            
            }
        }
    }
}